from __future__ import print_function

import time
import pigamepad
import sys

#Adding echo to print for python 2.7 compatibility
def echo(input):
    print(input)

A=pigamepad.SnesPad(r'/dev/input/js0',echo)
time.sleep(10)
print('done')
sys.stdout.flush()
