import c_pigamepad
import threading
import warnings


class PadEvent(object):
    BUTTONPRESS = 1
    AXISMOVE = 2
    INITBUTTON = 129
    INITAXIS = 130
    EXCEPTION = -1

    def __init__(self,time,value,eventype,number):
        self.allowedEvents = [self.BUTTONPRESS,self.AXISMOVE,self.INITBUTTON,self.INITAXIS,self.EXCEPTION]
        self.time = time
        self.value = value
        self.number = number
        self.eventype = eventype
        if not eventype in self.allowedEvents:
            warnings.warn("Unknown gampad event", Warning)

class PadThread(threading.Thread):
    def __init__(self, port,callback):
        self._port = port
        self._callback = callback
        threading.Thread.__init__(self)
        
    def run(self):
        try:
            c_pigamepad.monitor_pad(self.python_callback,self._port)
        except RuntimeError as e:
            self.python_callback(None, e, PadEvent.EXCEPTION, -1)
    
    def python_callback(self,time,value,eventype,number):
        self._callback(PadEvent(time,value,eventype,number))

class ButtonPress(object):
    def __init__(self,name):
        self.name=name
    
    def __str__(self):
        return "ButtonPress: %s"%self.name

class ButtonRelease(object):
    def __init__(self,name):
        self.name=name
    
    def __str__(self):
        return "ButtonRelease: %s"%self.name

class AxisUpdate(object):
    def __init__(self,name,value):
        self.name=name
        self.value=value
        
    def __str__(self):
        return "AxisUpdate: %s=%d"%(self.name,self.value)

class PadException(object):
    def __init__(self,name,value):
        self.name=name
        self.value=value
        
    def __str__(self):
        return "PadException: %s=%s"%(self.name, self.value)

class GamePad(object):
    def __init__(self,port,extcallback=None,paddef=None):
        self._port = port
        self._extcallback=extcallback
        
        self.axes={}
        self.buttons={}
        if paddef is None:
            self.buildonthefly = True
            self.defaxes={}
            self.defbuttons={}
        else:
            self.buildonthefly = False
            self.defaxes = paddef['axes']
            self.defbuttons = paddef['buttons']
            for key in self.defaxes.keys():
                self.axes[self.defaxes[key]['name']]=None
            
            for key in self.defbuttons.keys():
                self.buttons[self.defbuttons[key]['name']]=None
            
        self.t1 = PadThread(port, self.callback)
        self.t1.setDaemon(True)
        self.t1.start()

    @property
    def thread_alive(self):
        return self.t1.is_alive()

    def callback(self,event):
        #Building dictionary of buttons and axes as the code runs
        if self.buildonthefly:
            if event.eventype == event.INITAXIS:
                ax=str(event.number)
                if not ax in self.defaxes:
                    self.defaxes[ax]={'name':'axis-%s'%ax,'inv':False}
                    self.axes['axis-%s'%ax]=None
            elif event.eventype == event.INITBUTTON:
                button=str(event.number)
                if not button in self.defbuttons:
                    self.defbuttons[button]={'name':'button-%s'%button}
                    self.buttons['button-%s'%button]=None
        
        #Handle events
        if event.eventype == PadEvent.EXCEPTION:
            self._extcallback(PadException("exception",event.value))
        if event.eventype in [event.AXISMOVE,event.INITAXIS]:
            ax=str(event.number)
            if ax in self.defaxes:
                name = self.defaxes[ax]['name']
                if self.defaxes[ax]['inv']:
                    self.axes[name] = -event.value
                else:
                    self.axes[name] = event.value
                if (self._extcallback is not None) and (event.eventype == event.AXISMOVE):
                    self._extcallback(AxisUpdate(name,event.value))
        elif event.eventype in [event.BUTTONPRESS,event.INITBUTTON]:
            button=str(event.number)
            if button in self.defbuttons:
                name = self.defbuttons[button]['name']
                self.buttons[name] = event.value
                if (self._extcallback is not None) and (event.eventype == event.BUTTONPRESS):
                    if event.value:
                        self._extcallback(ButtonPress(name))
                    else:
                        self._extcallback(ButtonRelease(name))
                        
    
    def getstatus(self,event):
        return (self.axes,self.buttons)

class SnesPad(GamePad):
    
    def __init__(self,port,extcallback=None):
        paddef = {
            'axes':{
                '0':{'name':'x-ax','inv':False},
                '1':{'name':'y-ax','inv':True},
                },
            'buttons':{
                '0':{'name':'X'},
                '1':{'name':'A'},
                '2':{'name':'B'},
                '3':{'name':'Y'},
                '4':{'name':'Left Bumper'},
                '5':{'name':'Right Bumper'},
                '8':{'name':'select'},
                '9':{'name':'start'},
                }
            }
        GamePad.__init__(self,port,extcallback,paddef)
        print("I do have buildonthefly it is: "+str(self.buildonthefly))
    
class N64Pad(GamePad):
    
    def __init__(self,port,extcallback=None):
        paddef = {
            'axes':{
                '0':{'name':'a-stick x-ax','inv':False},
                '1':{'name':'a-stick y-ax','inv':True},
                '5':{'name':'d-pad x-ax','inv':False},
                '6':{'name':'d-pad y-ax','inv':True},
                },
            'buttons':{
                '0':{'name':'C-up'},
                '1':{'name':'C-right'},
                '2':{'name':'C-down'},
                '3':{'name':'C-left'},
                '4':{'name':'Left Bumper'},
                '5':{'name':'Right Bumper'},
                '6':{'name':'A'},
                '7':{'name':'Z'},
                '8':{'name':'B'},
                '9':{'name':'start'},
                }
            }
        GamePad.__init__(self,port,extcallback,paddef)
        print("I do have buildonthefly it is: "+str(self.buildonthefly))