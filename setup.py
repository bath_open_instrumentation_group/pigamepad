from setuptools import setup, find_packages
from distutils.extension import Extension

setup(
  name = 'pigamepad',
  packages = find_packages(),
  ext_modules = [Extension("c_pigamepad",
                           ["src/c_pigamepad.c"],
                           extra_compile_args=["-fopenmp"],
                           extra_link_args=["-fopenmp"]
)]
)
